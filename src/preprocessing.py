import re
import random
import argparse
from pathlib import Path

TARGET_PATTERN = r'(?<=<).*?(?=>)'     # use positive lookbehind, positive lookahead
CHUNK_PATTERN = r'({}|\w+)'.format(TARGET_PATTERN)  # target & (remaining) word

def read_text_lines(csv_file_path, column_head=False):
    csv_file_list = [csv_file_path] if type(csv_file_path) is str else csv_file_path

    text_lines = []
    for csv_file in csv_file_list:
        try:
            file_lines = open(csv_file, 'r').readlines()
        except:
            file_lines = open(csv_file, 'r', encoding='cp949').readlines()
        if column_head:
            file_lines = file_lines[1:]
        text_lines.extend(file_lines)
        
    return text_lines

def extract_entities(text_lines):
    # find entity_name
    entities = set()
    for text_line in text_lines:
        text = text_line.split('\t')[1]
        target_list = re.findall(TARGET_PATTERN, text)
        for target in target_list:
            entities.add(target.split('=')[0])
    return entities

# Generate entity dict from text
# return: dict. key: entity, value: set of corresponding texts
def generate_entity_texts_dict(text_lines):
    entity_texts_dict = {}
    for text_line in text_lines:
        text = text_line.split('\t')[1]
        target_list = re.findall(TARGET_PATTERN, text)

        for target in target_list:
            entity, entity_text = target.split('=')
            if entity not in entity_texts_dict:
                entity_texts_dict[entity] = set()
            entity_texts_dict[entity].add(entity_text)

    return entity_texts_dict

# Parse labels & words from a list of text lines
# maybe it's not the best way to parse labeled information
def parse_text(text_lines):
    words_list = []
    labels_list = []
    for text_line in text_lines:
        origin_text, labeled_text = text_line.split('\t')    # TODO change it to \t

        targets = re.findall(TARGET_PATTERN, labeled_text)
        labeled_chunks = re.findall(CHUNK_PATTERN, labeled_text)

        labels = []
        chunk_words = []
        for chunk in labeled_chunks:
            if chunk in targets:
                label, chunk_text = chunk.split('=')
                subchunks = chunk_text.split(' ')
                labels.append(label)
                chunk_words.append(subchunks[0])
                for subchunk in subchunks[1:]:
                    labels.append('-')
                    chunk_words.append(subchunk)
            else:
                labels.append('X')
                chunk_words.append(chunk)

        # Merge suffixes back, that might be split during the above pattern-matching.
        origin_words = re.findall(CHUNK_PATTERN, origin_text)
        origin_labels = []
        offset = 0
        for idx, origin_word in enumerate(origin_words):
            chunk_word = chunk_words[idx+offset]
            origin_labels.append(labels[idx+offset])
            while origin_word != chunk_word:
                offset += 1
                chunk_word += chunk_words[idx+offset]

        labels_list.append(origin_labels)
        words_list.append(origin_words)
    return words_list, labels_list


# Augment data by shuffling entity texts
def augment_text(text_lines, entity_texts_dict, augment_num):
    augments = []

    entities = entity_texts_dict.keys()
    for _ in range(augment_num):
        text_line = random.sample(text_lines, 1)[0]
        _, labeled_text = text_line.split('\t')

        targets = re.findall(TARGET_PATTERN, labeled_text)
        for target in targets:
            entity, entity_text = target.split('=')
            candidate_text = random.sample(entity_texts_dict[entity], 1)[0]
            augment = text_line.replace(entity_text, candidate_text)
            augments.append(augment)

    augmented_text_lines = text_lines + augments
    return augmented_text_lines


# make BERT_NER train & test file(txt)
def make_ner_input_file(words_list, labels_list, train_file_path, test_file_path, test_ratio=0.1):
    test_data_border = int(len(words_list) * test_ratio)

    data = list(zip(words_list, labels_list))
    random.shuffle(data)
    train_data, test_data = data[:-test_data_border], data[-test_data_border:]

    with open(train_file_path, 'w') as train_file:
        for train_record in train_data:
            for idx, (word, label) in enumerate(zip(*train_record)):
                train_file.write('{}\t{}\t{}\n'.format(idx, word, label))
            train_file.write('\n')

    if test_data == []:
        return

    with open(test_file_path, 'w') as test_file:
        for test_record in test_data:
            for idx, (word, label) in enumerate(zip(*test_record)):
                test_file.write('{}\t{}\t{}\n'.format(idx, word, label))
            test_file.write('\n')

if __name__== '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--csv_path', type=str, required=True)
    parser.add_argument('--output_train', type=str, required=True)
    parser.add_argument('--output_test', type=str, required=True)
    parser.add_argument('--augment_size', type=int, default=1000)
    parser.add_argument('--test_ratio', type=float, default=0.1)
    args = parser.parse_args()

    text_lines = read_text_lines(args.csv_path)
    entity_texts_dict = generate_entity_texts_dict(text_lines)
    if args.augment_size > 0:
        text_lines = augment_text(text_lines, entity_texts_dict, args.augment_size)
    words_list, labels_list = parse_text(text_lines)

    make_ner_input_file(words_list, labels_list, args.output_train, args.output_test, test_ratio=args.test_ratio)

    print('train file saved!')
    print('train file_path :', args.output_train)
    print('test file_path :', args.output_test)
