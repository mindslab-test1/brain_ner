# MindsLab Inc. 
# Named Entity Recognizer

import argparse, logging, sys, time, grpc
from concurrent import futures
from datetime import datetime
from omegaconf import OmegaConf
from pathlib import Path

from runner import NerRunner, setup_config, setup_logger, logger

from proto.ner_v2_pb2 import NerResponse, Entity
from proto.ner_v2_pb2_grpc import NamedEntityRecognitionServicer, add_NamedEntityRecognitionServicer_to_server

_ONE_DAY_IN_SECONDS = 60 * 60 * 24

class BertNER(NamedEntityRecognitionServicer):
    def __init__(self, cfg):
        self.runner = NerRunner(cfg)

        logger.info('Warming up the GPU with a dummy data')
        _ = self.runner.infer(['Dummy text for warmup'])
        logger.info('Done warming up')

    def GetNamedEntities(self, request, context):
        _input = request.text

        results = self.runner.infer([_input])
        logger.debug(results)

        output = self._postprocess(results[0])

        return output

    def _postprocess(self, result):
        print(result)
        if len(result[0]) > 0:
            entities = [Entity(text=text, category=category) for text, category in zip(*result)]
        else:
            entities = []
        output = NerResponse(entities=entities)
        return output


# define and read arguments especially for running, not modeling
def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--model_path', type=str, required=True)
    parser.add_argument('--log_level', type=str, default='INFO', choices=['INFO', 'DEBUG'])

    parser.add_argument('--port', type=int, required=True)

    args = parser.parse_args()
    return args


if __name__ == '__main__':
    args = parse_args()
    args.run = 'serve'

    cfg = setup_config(args)
    setup_logger(cfg)

    logger.info('Initializing ner engine')
    bert_ner = BertNER(cfg)

    logger.info('Building grpc server')
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1), )
    add_NamedEntityRecognitionServicer_to_server(bert_ner, server)

    server.add_insecure_port('[::]:{}'.format(args.port))
    server.start()

    logger.info('Ner Server starting at 0.0.0.0:{}'.format(args.port))

    try:
        while True:
            # Sleep forever, since `start` doesn't block
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        logger.info('Shutting down the server')
        server.stop(0)
