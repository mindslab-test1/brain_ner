# MindsLab Inc. 
# Named Entity Recognizer

import grpc
import argparse
from proto.ner_v2_pb2 import NerRequest
from proto.ner_v2_pb2_grpc import NamedEntityRecognitionStub

def request(text, host, port):
    with grpc.insecure_channel('{}:{}'.format(host, port)) as channel:
        stub = NamedEntityRecognitionStub(channel)
        ner_request = NerRequest(text=text)

        return stub.GetNamedEntities(ner_request)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--host', type=str, default="localhost")
    parser.add_argument('--port', type=int, required=True)
    parser.add_argument('--text', type=str, default="NER 테스트용 텍스트입니다. 원하는 텍스트를 입력하세요.")

    args = parser.parse_args()
    
    response = request(args.text, args.host, args.port)

    print('[Text]:\n {}'.format(args.text))
    print('[Entities]')
    for entity in response.entities:
        print('Text: {} \t| Category: {}'.format(entity.text, entity.category))
    if len(response.entities) == 0:
        print('No entity is recognized')
