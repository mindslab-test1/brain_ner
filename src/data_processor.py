# MindsLab Inc. 
# Named Entity Recognizer

import tensorflow as tf

import time, random, re, json, collections
import numpy as np
from sklearn.metrics import classification_report
from pathlib import Path
from tqdm import tqdm

import preprocessing
import component
from component import DataProcessor, InputExample
from trfms.bert_google_tf1 import tokenization
from trfms import utils

class NerDataProcessor():
    def __init__(self, cfg):
        self.cfg = cfg

        self.model_dir_path = Path(cfg.path.model_dir)

        self.tokenizer = tokenization.FullTokenizer(vocab_file=cfg.path.vocab_file, do_lower_case=cfg.model.do_lower_case)
        self._processor = NerRawDataProcessor(cfg)
        _, self.label2idx_dict = self._processor.get_labels()
        self.idx2label = { idx:label for label, idx in self.label2idx_dict.items() }

    def build_train_input_fn(self, train_cfg):
        cfg = self.cfg
        model_cfg = cfg.model
        is_training = True      # Now unnecessary, but for future generalization into 'build_input_fn'

        # Load data file into a list of InputExamples
        examples = self._processor.get_file_examples(train_cfg.data, phase='train')

        if is_training:
            seed = int(time.time()) % cfg.seed_bucket
            random.Random(seed).shuffle(examples)

        # Tokenize and write data into a serialized tfrecord file to improve process time
        data_path = Path(train_cfg.data)
        tfrecord_path = data_path.parent.joinpath(model_cfg.tokenizer, data_path.stem)
        if not tfrecord_path.exists():
            tfrecord_path.parent.mkdir(parents=True, exist_ok=True)

            component.file_based_convert_examples_to_features(
                    examples        = examples, 
                    max_seq_length  = model_cfg.max_seq_length, 
                    tokenizer       = self.tokenizer, 
                    output_file     = str(tfrecord_path),
                    label2id        = self.label2idx_dict)

        # Build input_fn using tfrecord
        input_fn = component.file_based_input_fn_builder(
            input_file      = str(tfrecord_path),
            seq_length      = model_cfg.max_seq_length,
            is_training     = is_training,
            drop_remainder  = is_training)

        return input_fn, examples

    # TODO maybe there is a better way 
    def build_infer_fn(self):
        model_cfg = self.cfg.model

        features = {
            'input_ids':   tf.placeholder(tf.int32, shape=[None, model_cfg.max_seq_length], name='input_ids'),
            'mask':        tf.placeholder(tf.int32, shape=[None, model_cfg.max_seq_length], name='mask'),
            'segment_ids': tf.placeholder(tf.int32, shape=[None, model_cfg.max_seq_length], name='segment_ids'),
            'label_ids':   tf.placeholder(tf.int32, shape=[None, model_cfg.max_seq_length], name='label_ids'),
        }
        infer_fn = tf.estimator.export.build_raw_serving_input_receiver_fn(features)

        return infer_fn

    def build_test_examples(self, test_data_file):
        examples = self._processor.get_file_examples(test_data_file, phase='test')
        return examples

    def build_infer_examples(self, infer_texts):
        examples = self._processor.get_infer_examples(infer_texts)
        return examples

    def build_infer_features(self, examples):
        model_cfg = self.cfg.model

        _features = component.convert_examples_to_features(examples, model_cfg.max_seq_length, self.tokenizer, self.label2idx_dict)

        features = {
            'input_ids': [],
            'mask': [],
            'segment_ids': [],
            'label_ids': [],
        }
        for feature in _features:
            features['input_ids'].append(feature.input_ids)
            features['mask'].append(feature.mask)
            features['segment_ids'].append(feature.segment_ids)
            features['label_ids'].append(feature.label_ids)

        return features

    def postprocess_batches(self, batched_inputs, batched_outputs):
        cfg = self.cfg

        # extend batched results into concatenated records
        inputs = []
        predictions = []
        for batched_input, batched_output in zip(batched_inputs, batched_outputs):
            pred_label_ids = batched_output['output']

            # the order of tuple elements is important. it's for efficiency to choose the tuple structure.
            # it's temporary way of branching between infer(ndim==2), test(ndim==1) 
            if pred_label_ids.ndim == 2:
                predictions.extend(zip(pred_label_ids))
                inputs.extend(batched_input)
            elif pred_label_ids.ndim == 1:
                predictions.append((pred_label_ids))
                inputs.append(batched_input)

        results = []
        for _input, prediction in zip(inputs, predictions):
            origin_texts = _input.text
            origin_labels = _input.label.split('\t')

            tokens = self.tokenizer.tokenize(origin_texts)

            (pred_label_ids,) = prediction
            pred_label_ids = pred_label_ids[1:1+len(tokens)]    # skipping [CLS]
            pred_labels = [self.idx2label[label_id] for label_id in pred_label_ids]  

            merged_tokens, categories = _merge_subtokens(tokens, pred_labels)

            results.append((merged_tokens, origin_labels, categories))

        return results

    def evaluate_result(self, results):
        cfg = self.cfg
        prediction_path = self.model_dir_path.joinpath(cfg.const.prediction)

        labels = []
        preds = []
        with prediction_path.open('w') as f:
            num_written_examples = 0
            score = 0
            for result_idx, result in tqdm(enumerate(results), desc='Writing result'):
                words, _labels, _preds = result

                # evaluate
                preds.extend(_preds)
                labels.extend(_labels)
                score += sum([pred == label for pred, label in zip(_preds, _labels)])

                # print
                f.write('[#{}] Text | Label | Pred\n'.format(result_idx+1))
                for word, label, pred in zip(words, _labels, _preds):
                    f.write('{}\t{}\t{}\n'.format(word, label, pred))
                f.write('\n')

                num_written_examples += 1

            cm_report = classification_report(labels, preds)
            f.write('\nAcc: {:.4f}\n\nClassification Report\n {}'.format(score / len(labels), cm_report))

        print('Acc: {:.4f}'.format(score / len(labels)))
        print('Classification Report: {}'.format(cm_report))
        assert num_written_examples == len(results)

    def filter_entity(self, infer_results):
        filtered_results = []
        for tokens, _, categories in infer_results:
            filtered_results.append(_filter_entity(tokens, categories, self._processor.essentials))

        return filtered_results


def _write_base(batch_tokens, id2label, prediction, batch_labels, wf, i):
    token = batch_tokens[i]
    predict = id2label[prediction]
    true_l = id2label[batch_labels[i]]
    if token!="[PAD]" and token!="[CLS]" and true_l!="-":
        #
        if predict=="-" and not predict.startswith("##"):
            predict="O"
        line = "{}\t{}\t{}\n".format(token,true_l,predict)
        wf.write(line)


def write_predict_file(output_predict_file, result, batch_tokens, batch_labels, id2label, crf):
    with open(output_predict_file, 'w') as wf:
        if crf:
            predictions = []
            for m, pred in enumerate(result):
                predictions.extend(pred)
            for i, prediction in enumerate(predictions):
                _write_base(batch_tokens, id2label, prediction, batch_labels, wf, i)
        else:
            for i, prediction in enumerate(result):
                _write_base(batch_tokens, id2label, prediction, batch_labels, wf, i)

def _merge_subtokens(tokens, pred_labels):
    # Detokenize & merge sub entity-words
    idx = 0
    merged_tokens = []
    categories = []
    while idx < len(tokens):
        pred_label = pred_labels[idx]
        token = tokens[idx]
        
        token_parts = [token]
        offset = 1
        while idx+offset < len(tokens) and tokens[idx+offset].startswith('##'):
            token_parts.append(tokens[idx+offset][2:])
            offset += 1

        merged_token = ''.join(token_parts)
        merged_tokens.append(merged_token)
        categories.append(pred_label)

        idx += offset

    return merged_tokens, categories

def _filter_entity(tokens, categories, essentials):
    _words = []
    _categories_by_word = []

    # Merge connected entity sub-words together
    for token, category in zip(tokens, categories):
        if category != '-':
            _words.append(token)
            _categories_by_word.append(category)
        elif len(_categories_by_word) > 1:
            _words[len(_words)-1] += ' ' + token

    words = []
    categories_by_word = []
    for word, category in zip(_words, _categories_by_word):
        if category not in essentials:
            words.append(word)
            categories_by_word.append(category)

    return words, categories_by_word


class NerRawDataProcessor(DataProcessor):
    """Processor for the NER data set """
    def __init__(self, cfg):
        self.cfg = cfg
        self.essentials = ['X', "-", "[CLS]", "[SEP]", "[PAD]"]
        self._prepare_labels()

    def _prepare_labels(self):
        cfg = self.cfg

        model_dir_path = Path(cfg.path.model_dir)
        label2idx_path = model_dir_path.joinpath(cfg.const.label2idx)
        if not label2idx_path.exists():     # first training phase
            lines = Path(cfg.train.data).read_text().replace('\n\n', '\n').split('\n')
            categories = set([line.split('\t')[-1] for line in lines])

            labels = categories.union(self.essentials)

            label2idx_dict = { label:idx for (idx, label) in enumerate(labels) }
            with label2idx_path.open('w') as f:
                json.dump(label2idx_dict, f)
        else:
            with label2idx_path.open('r') as f:
                label2idx_dict = json.load(f)
            labels = [label for (label, idx) in label2idx_dict.items()]

        self.labels = labels
        self.label2idx_dict = label2idx_dict

    def get_file_examples(self, data_file, phase=None):
        return self._create_examples(
            self._read_data(data_file), phase)
    
    def get_infer_examples(self, infer_texts):
        return self._create_examples(
            [(text, '\t'.join(['-' for token in text.split()])) for text in infer_texts], 'infer')

    def get_labels(self):
        return self.labels, self.label2idx_dict

    def _create_examples(self, utterances, set_type=None):
        examples = []
        for (i, utter) in enumerate(utterances):
            guid = "%s-%s" % (set_type, i)
            texts = tokenization.convert_to_unicode(utter[0])
            labels = tokenization.convert_to_unicode(utter[1])
            examples.append(InputExample(guid=guid, text=texts, label=labels))
        return examples
