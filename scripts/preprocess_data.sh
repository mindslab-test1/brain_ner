#!/bin/bash

TSV_PATH="data/sample/raw_data.tsv"
OUTPUT_TRAIN="data/sample/train_data.txt"
OUTPUT_TEST="data/sample/test_data.txt"
AUGMENT_SIZE=0
TEST_RATIO=0.1

python src/preprocessing.py \
    --csv_path ${TSV_PATH} \
    --output_train ${OUTPUT_TRAIN} \
    --output_test ${OUTPUT_TEST} \
    --augment_size ${AUGMENT_SIZE} \
    --test_ratio ${TEST_RATIO}
