#!/bin/bash
export CUDA_VISIBLE_DEVICES=6

MODEL_PATH="models/sample"

python src/runner.py --run infer \
    --model_path ${MODEL_PATH} \
    --infer_text "아메리카노 한잔 주세요." \
    --infer_text "따뜻한 라떼 한잔 주세요." \
