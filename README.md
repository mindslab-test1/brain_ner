Named Entity Recognizer
===
개체명 인식 엔진
- version: 1.1.0
- 예시 코드는 `data/sample` 아래에 커피 주문 대화 데이터가 위치한 예시로 작성됐음

#### * clone 시 `git clone --recursive {address}`를 통해 submodule까지 받도록 한다

## Docker how-to
1. train/test/inference/serve 개발용 이미지
    - 아래 명령어로 도커 실행
    ```
    docker run -it \
        --gpus "\"device={gpu_device}\"" \
        -v {host_pretrained_dir}:/pretrained \
        -v {host_models_dir}:/models \
        --name ner_engine \
        docker.maum.ai:443/brain/brain_ner:{version}
    ```
    - 예시(packed image를 사용)
    ```
    docker run -it \
        --gpus "\"device=0,2\"" \
        -v models:/models \
        --name ner_engine \
        docker.maum.ai:443/brain/brain_ner:1.1.0-packed
    ```
    - 이후 아래 설명에 따라 진행

2. server 전용 도커 이미지 사용
    ```
    docker run -d \
        --gpus "\"device={gpu_device}\"" \
        -v {host_model_path}:/model \
        -e MODEL_PATH=/model \
        -p {host_port}:{inner_port} \
        -e PORT={inner_port} \
        --name ner_server \
        docker.maum.ai:443/brain/brain_ner:{version}-server
    ```
---

### Train
1. Data 준비
    - `data/` 하위에 디렉토리를 생성하고 tsv 형식의 데이터를 넣는다.
    - 형식: Text + '\t' + Entity-tagged Text 
    - Entity tag는 `<COFFEE_TYPE=아메리카노> 주세요`와 같이 한다
    - `data/sample/raw_data.tsv` 참고

2. Data preprocessing (train.txt 생성)
    - `scripts/preprocess_data.sh` 수정 
        - `TSV_PATH`: data 경로
        - `OUTPUT_TRAIN`: 생성될 train data 경로
        - `OUTPUT_TEST`: 생성될 test data 경로
        - `AUGMENT_SIZE`: data augmentation 수행할 횟수
        - `TEST_RATIO`: 전체 데이터 중 test 데이터 비율(0이면 train 데이터만 생성)
    - preprocessing 실행
       ```python
       bash scripts/preprocess_data.sh
       ```

3. Config file 수정 (`cfg/config.yml`)
    - `train.pretrained_ckpt`: 사전학습 모델 경로
    - `train.vocab_file`: 사전학습시 사용한 vocab 파일 경로
    - `train.data`: 학습 데이터 경로
    - 그 외 `train.*`: 필요한 파라메터를 조정한다
    
4. Train script 수정 후 실행 (`scripts/run_train.sh`)
    - `export CUDA_VISIBLE_DEVICES=`: 사용할 GPU 번호 설정(ex. 0,1,2). 맨 앞을 주석으로 처리하면 모든 GPU 사용
    - `CFG=`, `MODEL_CFG=` 다른 config file 사용한다면 해당 경로. 기본값으로 둬도 된다.
    - `$ bash scripts/run_train.sh`로 실행

- 학습 후 `models/{model_name}`에 학습된 모델 checkpoint 생성. 


## Inference(bulk test, inference, serving) 공통
1. 실행할 때는 `scripts/run_{test,infer,server}.sh` 사용
    - 주요 argument는 `MODEL_PATH`이며, 필요한 config 파일 등은 해당 경로에서 모두 불러온다.
    - `MODEL_PATH=`: 학습된 모델이 위치된 폴더, 또는 특정 체크포인트까지의 경로. 폴더로 지정할 경우 가장 마지막 체크포인트 사용
        - ex) `models/{model_name}` or `models/{model_name}/model.ckpt-777`
    - 따라서 inference 설정을 변경하려면 `cfg/config.yml`이 아니라 `{model_path}/run_config.yml`을 수정해야 한다.
2. Config file 수정 (`models/{model_name}/run_config.yml`) 
    - `infer`: test, infer, serve 모두 공통으로 `infer` 하위의 파라메터들로 설정한다.
    - `infer.data`: bulk test용 데이터 위치 설정


### Bulk Test
1. Test script 수정 후 실행 (`scripts/run_test.sh`)
    - `export CUDA_VISIBLE_DEVICES=`: 사용할 GPU 번호(주석으로 처리하면 모든 GPU 사용)
    - `MODEL_PATH=`: 학습된 모델이 위치된 폴더, 또는 특정 체크포인트까지의 경로. 폴더로 지정할 경우 가장 마지막 체크포인트 사용
        - ex) `models/{model_name}` or `models/{model_name}/model.ckpt-777`
    - `$ bash scripts/run_test.sh`로 실행

- 테스트 후 `models/{model_name}`에 결과 파일(`prediction.txt`) 생성


### Inference
1. Inference script 수정 후 실행 (`scripts/run_infer.sh`)
    - `export CUDA_VISIBLE_DEVICES=`: 사용할 GPU 번호(주석으로 처리하면 모든 GPU 사용)
    - `--infer_text "text" \`: 확인하고자 하는 텍스트 추가(여러개를 추가할 수 있다)
    - `$ bash scripts/run_infer.sh`로 실행


### Serving
1. Server script 수정 후 실행 (`scripts/run_server.sh`)
    - `export CUDA_VISIBLE_DEVICES=`: 사용할 GPU 번호(주석으로 처리하면 모든 GPU 사용)
    - `MODEL_PATH=`: 모델 경로
    - `PORT=`: port number
    - `$ bash scripts/run_server.sh`로 실행


### Client Demo (Python)
1. Client script 수정 후 실행 (`scripts/run_client.sh`)
    - `PORT=`: port number
    - `--text "text"`: 확인하고자 하는 텍스트 입력
    - `$ bash scripts/run_client.sh`로 실행


### Troubleshoot
1. 테스트 데이터를 수정 후 업데이트 되지 않을 때,
    - `{data_dir}/wordpiece/test` 데이터 위치한 폴더에 자동으로 생성된 파일을 지우고 다시 실행
2. `$ bash scripts/{}.sh` 실행이 안될 때,
    - 인자를 입력하는 부분에서 `\`뒤에 공백이 없도록 주의
3. (deprecated) 조사 분리는 OKT 토크나이저의 어미, 조사 목록을 활용함(`endings.txt`에 임의의 항목 추가 가능)
    - 현재 사용하고 있지 않으며, 결과 데이터를 갖고 직접 구현하기를 권장.
